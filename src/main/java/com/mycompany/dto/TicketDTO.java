package com.mycompany.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TicketDTO {
    private SourceDTO source;
    private int id;
}
