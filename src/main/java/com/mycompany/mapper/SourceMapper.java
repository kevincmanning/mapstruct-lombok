package com.mycompany.mapper;

import com.mycompany.dto.SourceDTO;
import com.mycompany.model.Source;
import org.mapstruct.Mapper;

@Mapper
public interface SourceMapper {
    SourceDTO toDTO(Source s);
}
