package com.mycompany.model;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder(toBuilder = true)
public class Source {
    private String test;
    private String privateData;
}
