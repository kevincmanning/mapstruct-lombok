package com.mycompany.mapper;

import com.mycompany.dto.SourceDTO;
import com.mycompany.dto.SourceDTO.SourceDTOBuilder;
import com.mycompany.model.Source;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-09-12T14:09:36+0900",
    comments = "version: 1.3.0.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
public class SourceMapperImpl implements SourceMapper {

    @Override
    public SourceDTO toDTO(Source s) {
        if ( s == null ) {
            return null;
        }

        SourceDTOBuilder sourceDTO = SourceDTO.builder();

        sourceDTO.test( s.getTest() );

        return sourceDTO.build();
    }
}
