package com.mycompany.mapper;

import com.mycompany.dto.SourceDTO;
import com.mycompany.entities.Target;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-09-12T14:09:36+0900",
    comments = "version: 1.3.0.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
public class SourceTargetMapperImpl implements SourceTargetMapper {

    @Override
    public Target toTarget(SourceDTO s) {
        if ( s == null ) {
            return null;
        }

        Target target = new Target();

        if ( s.getTest() != null ) {
            target.setTesting( Long.parseLong( s.getTest() ) );
        }

        return target;
    }
}
