/* tslint:disable */
/* eslint-disable */
// Generated using typescript-generator version 2.16.538 on 2019-09-12 14:13:23.

export interface SourceDTO {
    test: string;
}

export interface TicketDTO {
    source: SourceDTO;
    id: number;
}
